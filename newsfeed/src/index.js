import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {setState} from './action_factory';
import io from 'socket.io-client';
import {createStore, applyMiddleware} from "redux";
import remoteActionMiddleware from './remote_action_middleware';
import reducer from './reducer';


const createStoreWithMiddleware = applyMiddleware(
    remoteActionMiddleware(socket)
)(createStore);
const store = createStoreWithMiddleware(reducer);

const socket = io(`${location.protocol}//${location.hostname}:8090`);
socket.on('state', state => {
        store.dispatch(setState(state));
        console.log(state);
    }
);


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
