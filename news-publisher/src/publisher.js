import {List, Map} from 'immutable';
import {get_endpoints} from "./config";
import {publish_news} from "./reducer"

const conf_file = "../app_conf.json";
const sockets = [];

export function publish(news){
    let publish_action =
        sockets.forEach(function(connection){ connection.emit(action) })
}

function gather_receivers(){
    let endpoints = get_endpoints(conf_file);
    endpoints.forEach(function (element){ sockets.push(create_socket(element)) })
}

function create_socket(endpoint){
    var socket = io(endpoint);
    return socket;
}