import {List, Map} from 'immutable';
import {get_news, set_news} from "./core";
export const INITIAL_STATE = Map();

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'SET_NEWS':
            return set_news(state, action.news);
        case 'GET_NEWS':
            return get_news(state);
    }
    return state;
}