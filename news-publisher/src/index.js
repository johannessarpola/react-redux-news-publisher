import React from 'react';
import makeStore from './store';
import {startServer} from './server';


export const store = makeStore();
startServer(store);


store.dispatch({
    type: 'SET_NEWS',
    news: require('../single-news.json')
});
store.dispatch({type: 'GET_NEWS'});