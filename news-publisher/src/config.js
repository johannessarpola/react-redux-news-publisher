/**
 * Created by Johannes on 25.3.2017.
 */
import * as fs from "fs";
function read_conf(conf) {
    let obj;
    obj = fs.readFileSync(conf, 'utf-8');
    return obj;
}

export function get_endpoints(conf){
    let obj = read_conf(conf);
    return obj['endpoints'];
}

