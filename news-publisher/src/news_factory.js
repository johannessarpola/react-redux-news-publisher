
export function create_news(meta, title, subtitle, body, author){
    let news = {
        meta : meta,
        title : title,
        subtitle : subtitle,
        body : body,
        author : author,
        date : new Date()
    };
    return news
}

