/**
 * Created by Johannes on 28.3.2017.
 */
import {List, Map} from 'immutable';

export function get_news(state) {
    const news = state.get('news');
    return news;
}

export function set_news(state, news) {
    return state.set('news', Map(news));
}

export function append_news(state, news) {
    return state.push('news', List(news));
}
